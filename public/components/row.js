function Row(datos){
    const {index, p , abonoP, i, pagosBancos, ingresos, egresos, pExtraordinarios} = datos;
    let id = new Date().getTime()+index;
    return (
        `
            <tr id="${id}">
                <th scope="row">${index}</th>
                <td>${toMoney(p)}</td>
                <td>${toMoney(abonoP)}</td>
                <td>${toMoney(i)}</td>
                <td>${toMoney(pagosBancos)}</td>
                <td>
                  <div class="dropdown">
                    <button class="btn btn-secondary dropdown-toggle" type="button" id="dropdownMenuButton1" data-bs-toggle="dropdown" aria-expanded="false">
                        Opciones
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenuButton1">
                        <li><div class="dropdown-item " onclick="agregarIngreso(${index})">Agregar ingreso</div></li>
                        <li><div class="dropdown-item " onclick="agregarEgreso(${index})">Agregar egreso</div></li>
                        <li><div class="dropdown-item bg-warning" onclick="agregarPExtraordinario(${index})">Agregar pago extraordinario</div></li>
                    </ul>
                  </div>
                </td>
                ${(ingresos)?Ingresos(ingresos, index):""}
                ${(egresos)?Egresos(egresos, index):""}
                ${(pExtraordinarios)?Extraordinarios(pExtraordinarios, index):""}
            </tr>
        `
    );
}

function Ingresos(ingresos, parentIndex){

    let info = "";

    ingresos.map(ingreso => {
        info +=
        `
            <tr>
                <td>${toMoney(ingreso)}</td>
                    <td>
                        <button class="btn btn-danger" onclick="eliminarIngreso('${ingreso}','${parentIndex}')" >X</button>
                </td>
            </tr>
        `
        return ingreso;
    })

    return (`
        <td>
            <table class="table table-success float-start">
                <tbody>                
                    ${
                        info
                    }
                </tbody>
            </table>
        </td>
    `);
}

function Egresos(egresos, parentIndex){
    let info = "";

    egresos.map(egreso => {
        info += 
        `
        <tr>
            <td>${toMoney(egreso)}</td>
                <td>
                    <button class="btn btn-danger" onclick="eliminarEgreso('${egreso}', '${parentIndex}')" >X</button>
                </td>
            </tr>
        `        
        return egreso;
    });

    return (`
        <td>
            <table class="table table-danger float-start">
                <tbody>                
                    ${
                        info
                    }               
                </tbody>
            </table>
        </td>
    `);
}

function Extraordinarios(pExtraordinarios, parentIndex){
    let info = "";

    pExtraordinarios.map(valor => {
        info += 
        `
        <tr>
            <td>${toMoney(valor)}</td>
                <td>
                    <button class="btn btn-danger" onclick="eliminarPExtraordinario('${valor}', '${parentIndex}')" >X</button>
                </td>
            </tr>
        `        
        return valor;
    });

    return (`
        <td>
            <table class="table table-warning float-start">
                <tbody>                
                    ${
                        info
                    }               
                </tbody>
            </table>
        </td>
    `);
}


function sumaAvariable(info){
    let totalAbonoP = 0;
    let totalI = 0;
    let totalPagoBancos = 0;

    info.map(dato => {
        totalAbonoP += dato.abonoP;
        totalI += dato.i;
        totalPagoBancos += dato.pagosBancos;
    });

    return(
        `
            <tr>
                <th scope="row">${info.length}</th>
                <td>0</td>
                <td></td>
                <td>0</td>
                <td>0</td>
            </tr>

            <tr>
                <th scope="row">TOTAL</th>
                <td></td>
                <td>${toMoney(totalAbonoP)}</td>
                <td>${toMoney(totalI)}</td>
                <td>${toMoney(totalPagoBancos)}</td>
            </tr>
        `
    )
}