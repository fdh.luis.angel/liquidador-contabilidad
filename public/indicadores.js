var infoAmortizacion = [];
var valoresFNE = [];

function calcularPeriodos(){    
    infoAmortizacion = [];
    let periodos = getTiempo()*getTipoPago()+1;
    let P = 0;
    let I = 0;
    let pagobanco = 0;
    let abonop = parseInt(getInversion() / (periodos-1));

    for (let index = 0; index < periodos; index++) {
        let datos = {}
        

        if(index === 0){
            datos = {index:index, p:getInversion(), abonoP:0, i:0, pagosBancos:0, ingresos: [], egresos:[], pExtraordinarios:[]};
            P = getInversion();
        }
        else if(index === 1){
            I = parseInt(P*getInteres());
            pagobanco = abonop + I;
            datos = {index:index, p:P, abonoP:abonop, i:I, pagosBancos:pagobanco, ingresos: [], egresos:[], pExtraordinarios:[]};
        }
        else{
            P = P-abonop;

            I = parseInt(P*getInteres());
            pagobanco = abonop + I;
            datos = {index:index, p:P, abonoP:abonop, i:I, pagosBancos:pagobanco, ingresos: [], egresos:[], pExtraordinarios:[]};
        }

        // ------------------------------------------------------   Ingresos fijos
        if(index> 0 && getIngresoFijo()){
            datos.ingresos.push(getIngresoFijo());
        }

        if(index> 0 && getEgresoFijo()){
            datos.egresos.push(getEgresoFijo());
        }
        //-------------------------------------------------------------------------
        infoAmortizacion.push(datos);
    }
    mostrarTabla();
}

function mostrarTabla(){
    resetButtons();
    tabla_avariable.innerHTML = "";
    let innerhtml = "";
    
    infoAmortizacion.forEach(datos => {
        innerhtml += Row(datos);
    });

    innerhtml += sumaAvariable(infoAmortizacion);
    tabla_avariable.innerHTML = innerhtml;

    llenarFNE();
    mostrarTablaFNE();
    loadChart();
}


function recalcularPeriodos(){    
    let previousP = undefined;
    let inversion = getInversion();
    let periodos = getTiempo()*getTipoPago();
    let abonop = parseInt(inversion / periodos);
    
    infoAmortizacion.forEach((dato, i) => {
        
        if(i > 1){
            dato.p = previousP - abonop;

            dato.pExtraordinarios.map((pago, j) => {
                dato.p = dato.p - pago; 
                
                inversion = dato.p;
            }); 

            if(dato.pExtraordinarios.length > 0)periodos -= (i-1);

            abonop = parseInt(inversion / periodos);
            dato.abonoP = abonop;

            dato.i = parseInt(dato.p*getInteres());
            dato.pagosBancos = dato.abonoP + dato.i;
            previousP = dato.p;
        }else{
            previousP = dato.p;
        }
    });

    mostrarTabla();
}


//--------------------------------------------------------------------------------------------------------- FUNCIONES DE ROWS

function agregarIngreso(index){
    let valor = prompt("Escriba un ingreso");
    valor?infoAmortizacion[index].ingresos.push(parseFloat(valor)): console.log("Debe ingresar un valor válido");
    mostrarTabla();
}

function agregarEgreso(index){
    let valor = prompt("Escriba un egreso");
    valor?infoAmortizacion[index].egresos.push(parseFloat(valor)): console.log("Debe ingresar un valor válido");
    mostrarTabla();
}

function eliminarIngreso(value, index){
    infoAmortizacion[index].ingresos = infoAmortizacion[index].ingresos.filter(dato => dato !== parseFloat(value));
    mostrarTabla();
}

function eliminarEgreso(value, index){
    infoAmortizacion[index].egresos = infoAmortizacion[index].egresos.filter(dato => dato !== parseFloat(value));
    mostrarTabla();
}

function agregarPExtraordinario(index){
    let valor = prompt("Escriba el valor del pago extraordinario");
    valor?infoAmortizacion[index].pExtraordinarios.push(parseFloat(valor)): console.log("Debe ingresar un valor válido");
    console.log("OK");
    recalcularPeriodos();
    //mostrarTabla();
}

function eliminarPExtraordinario(value, index){
    infoAmortizacion[index].pExtraordinarios = infoAmortizacion[index].ingresos.filter(dato => dato !== parseFloat(value));
    mostrarTabla();
}

//-------------------------------------------------
function toMoney(data){
   
    let aux = "";
    let counter = 0;
    let result = "";
    
    if(data < 0){
        result += "-";
        data = data*-1;
    }

    let value = "" + data;

    for (let i = value.length-1; i >= 0; i--) {
        const element = value[i];
        aux += element;
        
        if(counter === 2 && value[i-1]){
            aux += ".";
            counter = 0;
        }
        else counter += 1;
    }

    for (let i = aux.length-1; i >= 0; i--) {
        result += aux[i];
    }
    return result;
}

//--------------------------------------------------------------------- TABLA 2

function llenarFNE(){
    valoresFNE = [];
    let vpn = 0;

    if(!getTIO()){
        alert("No se puede calcular, debe ingresar PR y DTF");
        return;
    }

    infoAmortizacion.forEach((datos, i) => { 
        let ingresos = subTotal( datos.ingresos);     
        let egresos = subTotal(datos.egresos) + datos.pagosBancos;


        egresos += subTotal(datos.pExtraordinarios);
        
        let FNE = ingresos - egresos;

        let aux = {
            index: datos.index,
            ingresos : ingresos,
            egresos : egresos,
            fne: FNE,
            va : 0
        };

        if(i === 0){
            aux.egresos = (getInversion() + egresos);
            aux.fne = (-1 * (getInversion() + egresos))
        }
        else if(i === infoAmortizacion.length-1){
            ingresos += getRecuperacion();  
            aux.fne = (ingresos - egresos);
            aux.ingresos = ingresos;
        }

        if(i>0){
            aux.va = (aux.fne / ((1+getTIO())**datos.index));
            aux.va = parseInt(aux.va);

            vpn += aux.va;
        }

        valoresFNE.push(aux);
    });

    let VPN = (vpn - valoresFNE[0].egresos);

    if(VPN<0){
        btn_vpn.classList.remove("btn-success");
        btn_vpn.classList.add("btn-danger");
    }

    

    txt_vpn.value = toMoney(VPN);
    txt_tir.value = calculaTir()*100;

    txt_bc.value = ((VPN / (VPN - valoresFNE[0].egresos)));

    
}


function mostrarTablaFNE(){

    tabla_ingresos.innerHTML = "";
    let innerhtml = "";
    
    valoresFNE.forEach((datos) => {        
        innerhtml += RowIE(datos);
    });

    tabla_ingresos.innerHTML = innerhtml;
}

function subTotal(valores){
    let total = 0;

    valores.map(valor => {
        total += valor;
        return valor;
    })

    return total;
}


function calculaTir(){
    let resultado = 0;
    let error = 100;
    let TIR = 0;


    for (let decima = 1.0; decima < 20; decima+= 0.1) {
        let tir = 1+(decima/100);
        
        resultado = 0;
        valoresFNE.map((dato, i) => {
            if(i > 0){
                resultado += (dato.fne/ ((tir)**(i)));
            }
        }); 
        
        let localError = Math.abs((resultado - valoresFNE[0].egresos) / valoresFNE[0].egresos) * 100;

        if (localError < error) {
            TIR = tir;
            error = localError;
        }
    }

    for (let decima = 1.0; decima < 20; decima+= 0.1) {
        let tir = -(1+(decima/100));
        
        resultado = 0;
        valoresFNE.map((dato, i) => {
            if(i > 0){
                resultado += (dato.fne/ ((tir)**(i)));
            }
        }); 
        
        let localError = Math.abs((resultado - valoresFNE[0].egresos) / valoresFNE[0].egresos) * 100;

        if (localError < error) {
            TIR = tir;
            error = localError;
        }
    }
    
    //Establecer el color rojo si el tir es negativo
    if((TIR-1)<0){
        btn_tir.classList.remove("btn-success");
        btn_tir.classList.add("btn-danger");
    }

    return(TIR-1);   
}


//-----------------------------------------------------
function loadChart(){

    let labels = [];
    let ingresos = [];
    let egresos = [];
    
    valoresFNE.map((dato, i) => {
        labels.push(`${i}`);

        ingresos.push(dato.ingresos);
        egresos.push(-1*dato.egresos);
    });

    Object.assign(barCharData, {
      labels,
      datasets: [{
        label: 'Ingresos',
        backgroundColor: 'rgb(51, 209, 255)',
        borderColor: 'rgb(51, 147, 255)',
        data: ingresos
      },{
        label: 'Egresos',
        backgroundColor: 'rgb(244, 66, 66)',
        borderColor: 'rgb(230, 50, 50)',
        data: egresos
      }]
    });

    window.chart.update();
}
