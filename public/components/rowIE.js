function RowIE(datos){
    const {index, ingresos, egresos, fne, va} = datos;
    let id = new Date().getTime()+index;
    return (
        `
            <tr id="${id}">
                <th>${index}</th>
                <td>${toMoney(ingresos)}</td>
                <td>${toMoney(egresos)}</td>
                <td>${toMoney(fne)}</td>
                <td>${toMoney(va)}</td>
            </tr>
        `
    );
}



